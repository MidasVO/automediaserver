# Auto Media Server #

A comprehensive guide to setting up an automated media server. The aim of this project is to educate and inform. Anyone is free, and is encouraged to contribute. This is guide is allowed to be copied, shared, distributed but **NOT** sold. If you have anything to contribute create an issue. Thanks!

## Project ##

This project is developed on Bitbucket and hosted on http://automediaserver.space. The guide is written in English using Markdown for styling and mkdocs for serving. 

### Running the website ###

To run your own automediaserver.space website you need python 2.7x and mkdocs. Clone the repository and run **mkdocs serve**.