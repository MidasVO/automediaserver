
# Quick-Guide #

This guide assumes you have a fresh install of [Ubuntu Server](http://www.ubuntu.com/download/server)  14.04 LTS and are connected through SSH.

Please be aware that there are many ways to set this system up. For example, in this guide we will simply let SickRage save torrents to a folder which Deluge monitors. You can also let SickRage send the torrents to the download client but I had trouble with it so I opted for the black hole approach. You can change these settings at any time. If you feel lost at any point please check out the *Beginners' Guide* where I discuss the programs and reasoning in depth. I have tried to keep this version as minimal as possible but there is still a paragraph here and there where I felt it was needed. 


Update, upgrade and reboot:

> sudo apt-get update && sudo apt-get upgrade -y && sudo reboot


### Dependencies ###

> sudo apt-get install nano -y && sudo apt-get install curl -y && sudo apt-get install unzip -y

> sudo apt-get install software-properties-common -y 

> sudo add-apt-repository ppa:webupd8team/java -y

> sudo apt-get update && sudo apt-get install oracle-java8-installer -y 

Break here for accepting the Java terms.	

> sudo apt-get install oracle-java8-set-default -y
	

## FileBot ##

> sudo mkdir /var/lib/filebot/ && wget -qO ~/filebot.zip http://downloads.sourceforge.net/project/filebot/filebot/FileBot_4.5.6/FileBot_4.5.6-portable.zip && unzip -qo ~/filebot.zip -d /var/lib/filebot && rm -f ~/filebot.zip

Configure opensubtitles:


<< Configure alias for filebot here maybe, though for automation it's not really required as it is scripted anyways >>


> /var/lib/filebot/filebot.sh -script fn:configure

Install audio recognition software:

> sudo apt-get install libchromaprint-tools -y

## Deluge ##

Create the user:

> sudo adduser --disabled-password --system --home /var/lib/deluge --gecos "Deluge server" --group deluge
	

FileBot will not work without sudo priv so give them to deluge user, if anyone has better solution contact me:


<< Do an install without sudo adduser deluge not sure if its needed after chmodding >>

> sudo adduser deluge sudo


Depending on how your drive is partitioned create the */storage/* folder. The way we set things up you need a */finished/*, */unfinished/* and */torrents/* folder. Make sure there is enough space for incoming downloads. Deluge triggers a filebot script which copies/renames the files and puts them in the correct */storage/TV Shows/* folder.

Create the *storage*, *scripts* and *torrents* folder:

> sudo mkdir /home/storage/ && sudo mkdir /var/lib/deluge/scripts/

> sudo mkdir /var/lib/deluge/torrents/ && sudo chmod 775 /var/lib/deluge/torrents/

Create the */unfinished/* and */finished/* folders:

> sudo mkdir -p /home/storage/downloads/unfinished && sudo mkdir -p /home/storage/downloads/finished && sudo chown -R deluge:deluge /home/storage/downloads/



Install:

> sudo touch /var/log/deluged.log && sudo touch /var/log/deluge-web.log

> sudo chown deluge:deluge /var/log/deluge*

> sudo apt-get install deluged -y && sudo apt-get install deluge-webui -y 

Enable run at startup with deluge user:

> sudo nano /etc/default/deluge-daemon

Paste:

> DELUGED_USER="deluge"

> RUN_AT_STARTUP="YES"


Own the FileBot folder to let the Deluge script use the tool:

> sudo chown deluge:deluge /var/lib/filebot/ -R


Open daemon startup script:

> sudo nano /etc/init.d/deluge-daemon

Paste the contents of the following script:

http://pastebin.com/raw.php?i=4Symf1Y3


Make it executable:

> sudo chmod a+x /etc/init.d/deluge-daemon && sudo update-rc.d deluge-daemon defaults



##### Deluge post-processing script #####


Create post-processor 
	
> sudo nano /var/lib/deluge/scripts/deluge-postprocessor  
	
At this point you can create your own post-processor *or* use the one I supply you with. The file below searches the */home/storage/downloads/finished* folder for any files **not** in the amc.txt. It renames, downloads English subtitles, artwork and extras. The output folder is */home/storage/* in which it creates a folder called *TV Shows*. The path to an episode of a tv show would be */home/storage/TV Shows/Show/Season 01/Episode.S01E01.mp4*. Finally it creates a symbolic link to the sickrage tv folder.

Deluge-Postprocessing script: http://pastebin.com/raw.php?i=eithkff0

Make it executable:

> sudo chmod +rx /var/lib/deluge/scripts/deluge-postprocessor



After reboot go to:

	http://yourhost.com:8112

- Change the password
- Download to: */home/storage/downloads/unfinished/*
- Move Completed to: */home/storage/downloads/finished/*
- Auto-add torrents from: */var/lib/deluge/torrents*
- Enable Execute and Label plugins


Add the Torrent Completed event with this script in Deluge:
	
>  /var/lib/deluge/scripts/deluge-postprocessor  



## SickRage ##

> sudo apt-get install python-cheetah unrar-free git -y

> sudo adduser --disabled-password --system --home /var/lib/sickrage --gecos "SickRage" --group sickrage

> sudo git clone http://github.com/SiCKRAGETV/SickRage.git /var/lib/sickrage

> sudo chown sickrage:sickrage -R /var/lib/sickrage


Let's make it start on boot.

> sudo apt-get install upstart -y && sudo nano /etc/init/sickrage.conf

Paste:

> setuid sickrage

> start on runlevel [2345]

> stop on runlevel [016]

> respawn

> exec /var/lib/sickrage/SickBeard.py

Create the SickRage tv dir:
	
> sudo mkdir /var/lib/sickrage/tv/ && sudo chown sickrage:sickrage /var/lib/sickrage/ -R && sudo chmod 775 /var/lib/sickrage/tv/ && sudo reboot


After reboot go to:

	http://yourhost:8081

and set 

> /var/lib/sickrage/tv/

as the root directory of SickRage.

Set a username & password for SickRage.

Black hole to:

> /var/lib/deluge/torrents


Add providers to Search Providers such as kickass, rarbg and tpb. There are a lot of options, fiddle around. 



- Set search providers

Add a show!

## Plex ##

>sudo curl http://shell.ninthgate.se/packages/shell-ninthgate-se-keyring.key | sudo apt-key add -

> echo "deb http://www.deb-multimedia.org wheezy main non-free" | sudo tee -a /etc/apt/sources.list.d/deb-multimedia.list

> echo "deb http://shell.ninthgate.se/packages/debian wheezy main" | sudo tee -a /etc/apt/sources.list.d/plex.list

Update package list and install keyring:

> sudo apt-get update && sudo apt-get install deb-multimedia-keyring -y --force-yes 

Update package list and install Plex Media Server:

> sudo apt-get update && sudo apt-get install plexmediaserver -y

#### Activating Plex ####

Because we are on a headless server we need to use a simple trick to activate Plex. This is needed because the Plex Media Server requires the activation to be done on the machine itself, with its' web interface. Our solution is to create a tunnel into the headless server on a specific port which will allow us to use the machine remotely.

##### Windows: #####
With KiTTY connect to your server. Right click the KiTTY windows and in the context menu click on:

> 'Change Settings...'

Go to:

> -> Connection -> SSH -> Tunnels

Use: 

> Source port: 32400

> Destination: localhost:32400

~~
##### Linux / OSX: #####

Open a terminal. Enter:

> ssh root@yourhost.com -L 32400:localhost:32400 

Open your webbrowser and go to:

> http://localhost:32400/web/
~~

#### Remote Access ####

> http://localhost:32400/web/index.html#!/settings/server

Menu option:

> -> Remote Access

Sign into your Plex account. If you don't have one yet, register now.

In *Settings* - > *Remote Connection* enable: (you need to click show advanced)

> 'Manually specify port' 

at: 
	
> 32400

and hit 'Apply'.


#### Adding our Libraries ####

Alrighty! We can now go to:

> http://yourhost:32400/web/

and start adding content. Make sure you've downloaded some TV-Shows with SickRage. ;)

Click on the little plus sign and hit TV Series.

Add the map

> /storage/TV Shows/

Update your library and test out the media!!