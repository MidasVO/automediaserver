# Auto Media Server

The full guide to an automated media server using **Plex** for media streaming, **SickRage** for indexing TV Shows and grabbing torrents, **Deluge** to download the torrents, and **FileBot** to process the files.

Our server OS is Ubuntu 14.04 LTS, you will not need physical access to the server because throughout this guide we will use the commandline to connect to our server and install packages, and web-interfaces to configure the programs.

**This guide is still in progress and I will be accepting any help, feedback and criticism. You can create issues on the [Bitbucket repository](https://bitbucket.org/MidasVO/automediaserver/), create a thread on Reddit, or e-mail me directly.**

This website is meant purely educational and you are allowed to copy, modify and redistribute. You are **NOT** allowed to ask money for this guide!

### What? ###

We are using a dedicated machine connected to the internet to: 

- Index media
- Download media
- Rename media
- Download subtitles
- Download artwork
- Serve media via web or native applications

### Beginners' Guide ###

This version of the guide is aimed at the people who have no previous experience with linux and the command line. It contains detailed explanations and visual aids.

[Find the Beginners' Guide here.](/beginnersguide/) 

### Quick-Guide ###

The Quick-Guide is aimed at people who have been through this process before. It has minimal clutter and is straight to the point assuming you know how the basics.

### Video Guide ###

When this guide reaches a point of stability I aim to create a concise video tutorial of the entire process. 

### Contribute ###

Contributions are welcomed and come in the form of:

- Constructive criticism through bitbucket issues
- Discussions on [reddit](http://www.reddit.com/r/AutoMediaServer) 
- Pull requests with grammatical improvements and other style improvements
- Pull requests with new pages or features
