## Beginners' Guide ##

This guide assumes you have no previous knowledge of Linux and the programs we will use. If you're a beginner and you struggle at some point in this guide, please send your feedback so I can make it better.

### Our Goal ###

At the end of this guide we can call ourselves proud owners of a fully automated media download and streaming server. The aim is to be able to sit back and view our media content on any device without having to go through the effort of finding the media. 

### Key Components ###

The key components we will be trying to combine are:

- Plex 
	Serves content
- SickRage
	Index TV Shows and has knowledge of search providers
- Deluge
	Torrent downloading program. Offers a daemon, web-ui, and plugins
- FileBot
	Renames, moves, and downloads extras.


#### Understanding the different components ####

To start off we need a **server** and a **client**. The **server** is a dedicated computer which serves our files. The **client** is the device that gets served. So in order to watch your TV shows you will need a server which contains the episodes and a client which will connect to your server to request a stream of the content. 

##### Server #####


The requirements for our downloading and indexing programs are not high, however Plex is. 